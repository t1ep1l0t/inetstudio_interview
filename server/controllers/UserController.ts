import UserService from "../services/userService";
class UserController {
    async createUser(req, res) {
        try {
            const {
                title,
                subtitle,
                country,
                score,
                address
            } = req.body;

            const picture = req.files?.picture;

            const result = await UserService.createUser({
                title,
                subtitle,
                country,
                score,
                address
            }, picture);

            res.status(201).json({
                user: result,
                message: 'Success'
            })
        } catch (e) {
            console.log(e)
            res.status(500).json({
                message: 'Error',
                error: e
            })
        }
    }
    async deleteUser (req, res) {
        try {
            const id = req.params.id;
            if (!id) {
                return res.status(400).json({
                    message: 'ID is required',
                    error: 'BadRequest'
                })
            }
            const result = await UserService.deleteUser(id);

            res.status(201).json({
                message: `Success! User with ID ${result.id} deleted.`,
            })
        } catch (e) {
            console.log(e);
            res.status(404).json({
                message: `Error! User with ID ${req.params.id} not found.`,
            })
        }
    }
    async getAll (req, res) {
        const result = await UserService.getAll()

        res.status(200).json({
            users: result,
            message: 'Success'
        })
    }
}

export default new UserController();