import FileService from "./fileService";
import {PrismaClient} from '@prisma/client'

class UserService {

    private readonly prisma = new PrismaClient();
    async createUser({  title,
                         subtitle,
                         country,
                         score,
                         address
                     }, file :File) {
        let picture :string;

        if (file) {
            picture = FileService.saveFile(file);
        }

        if (!title || !country || !score || !address) {
            throw new Error('Title, country, score, address is required!')
        }

        return this.prisma.user.create({
            data: {
                title: title,
                subtitle: subtitle ? subtitle : undefined,
                country: country,
                score: Number(score),
                address: address,
                avatar: picture ? picture : null
            }
        });
    }
    async deleteUser (id :number) {
        try {
            const user = await this.prisma.user.delete({ where: {id: Number(id)}});
            if (user.avatar) {
                FileService.deleteFile(user.avatar);
            }
            return user

        } catch (e) {
            console.log(e)
        }
    }
    async getAll () {
        return this.prisma.user.findMany()
    }
}

export default new UserService();