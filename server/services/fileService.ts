import * as path from 'path';
// @ts-ignore
import fs from 'fs';
import * as uuid from 'uuid';
class FileService {
    saveFile (picture) {
        try {
            const fileName = uuid.v4() + `.${picture.mimetype.split('/')[1]}`;
            const filePath = path.resolve('./static/', fileName);
            picture.mv(filePath);
            return fileName;
        } catch (e) {
            console.log(e);
        }
    }
    deleteFile (file_name) {
        fs.unlink(path.resolve(`./static/${file_name}`), err => {
            if(err) throw err; // не удалось удалить файл
            console.log('Файл успешно удалён');
        });
        return file_name
    }
}

export default new FileService();