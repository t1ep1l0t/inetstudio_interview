-- CreateTable
CREATE TABLE "User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "avatar" TEXT,
    "title" TEXT NOT NULL,
    "subtitle" TEXT,
    "country" TEXT NOT NULL,
    "score" INTEGER NOT NULL,
    "address" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "User_title_key" ON "User"("title");
