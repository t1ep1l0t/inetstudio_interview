import express from 'express';
import {PrismaClient} from "@prisma/client";
import cors from 'cors'
import fileUpload from 'express-fileupload';
import UserController from "./controllers/UserController";
import * as path from "path";


const app = express();
const PORT = 3000;
const prisma = new PrismaClient;

app.use(cors());
app.use(express.json());
app.use(fileUpload());

app.use('/static', express.static(path.resolve('./static')))
app.get('/api/users', UserController.getAll);
app.post('/api/users', UserController.createUser);
app.delete('/api/users/:id', UserController.deleteUser);

const startApp = async () => {
    try {
        await prisma.$connect();
        app.listen(PORT, () => console.log(`Server started on http://localhost:${PORT}`))
    } catch (e) {
        console.log(e)
    }
};

startApp();