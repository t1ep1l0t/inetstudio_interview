import { defineStore } from 'pinia'

export const useApp = defineStore('app', {
  state: () => {
    return {
      createUserModal: false as boolean,
      isLoading: false as boolean
    }
  },
  actions: {
    setCreateUserModal() {
      this.createUserModal = !this.createUserModal
    }
  }
})
