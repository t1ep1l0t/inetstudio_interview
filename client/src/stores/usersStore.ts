import { defineStore } from 'pinia'
import type { IUser } from '@/dto/user.dto'
import type { IFilterState } from '@/dto/FilterState.dto'

const base_api_url = 'http://localhost:3000/api/users/'

export const useUsers = defineStore('users', {
  state: () => {
    return {
      users: [] as IUser[],
      filteredUsers: [] as IUser[],
      currentScore: {
        name: 'All',
        value: 'all'
      } as IFilterState,
      currentCountry: {
        name: 'All',
        value: 'all'
      } as IFilterState,
      isLoading: false as boolean
    }
  },
  actions: {
    async fetchUsers(): Promise<void> {
      this.setIsLoading()
      setTimeout(async () => {
        const response = await fetch(`${base_api_url}`)

        this.users = (await response.json()).users
        this.filteredUsers = this.users

        this.setFilteredUsers()
        this.setIsLoading()
      }, 1000)
    },
    setCurrentScore(newScore: IFilterState): void {
      this.currentScore = newScore
    },
    setCurrentCountry(newCountry: IFilterState): void {
      this.currentCountry = newCountry
    },
    setFilteredUsers(): void {
      if (this.currentCountry.value === 'all') {
        switch (this.currentScore.value.split(' ')[0]) {
          case '<':
            this.filteredUsers = this.users.filter(
              (user) => user.score < Number(this.currentScore.value.split(' ')[1])
            )
            break
          case '>':
            this.filteredUsers = this.users.filter(
              (user) => user.score > Number(this.currentScore.value.split(' ')[1])
            )
            break
          case 'all':
            this.filteredUsers = this.users
            break
        }
      } else {
        switch (this.currentScore.value.split(' ')[0]) {
          case '<':
            this.filteredUsers = this.users.filter(
              (user) =>
                user.country.toLowerCase() === this.currentCountry.value.toLowerCase() &&
                user.score < Number(this.currentScore.value.split(' ')[1])
            )
            break
          case '>':
            this.filteredUsers = this.users.filter(
              (user) =>
                user.country.toLowerCase() === this.currentCountry.value.toLowerCase() &&
                user.score > Number(this.currentScore.value.split(' ')[1])
            )
            break
          case 'all':
            this.filteredUsers = this.users.filter(
              (user) => user.country.toLowerCase() === this.currentCountry.value.toLowerCase()
            )
            break
        }
      }
    },

    async createUser(user: any, picture?: File) {
      const formData = new FormData()

      formData.append('title', user.title)
      formData.append('subtitle', user.subtitle ? user.subtitle : '')
      formData.append('country', user.country)
      formData.append('score', user.score)
      formData.append('address', user.address)

      if (picture) {
        formData.append('picture', picture)
      }

      const response = await fetch(base_api_url, {
        method: 'POST',
        headers: {
          ContentType: 'multipart/form-data'
        },
        body: formData
      })

      if (response.status === 201) {
        await this.fetchUsers()
      }
    },
    async deleteUser(id: number) {
      const response = await fetch(`${base_api_url + id}`, {
        method: 'DELETE'
      })

      if (response.status === 201) {
        this.users = this.users.filter((user) => user.id !== id)
        this.filteredUsers = this.filteredUsers.filter((user) => user.id !== id)
      }
    },
    setIsLoading() {
      this.isLoading = !this.isLoading
    }
  }
})
