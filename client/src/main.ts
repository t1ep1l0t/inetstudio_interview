import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import vuetify from '@/plugins/vuetify.config'

import '@/assets/main.scss'

const app = createApp(App)

app.use(createPinia()).use(vuetify).mount('#app')
