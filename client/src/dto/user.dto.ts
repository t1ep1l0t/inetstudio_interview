export interface IUser {
  avatar?: string
  title: string
  subtitle?: string
  country: string
  score: number
  address: string
  isAddressVisible?: boolean
  showFullSubtitle?: boolean
  id?: number
}
