# Тестовое задание для [Inetstudio](https://www.inetstudio.ru/)

## Default run app

### Run API

```bash
$ cd server
```
```bash
$ npm install
```
```bash
# development
$ npm run dev

# production
$ npm run start
```

### Run client

```bash
$ cd client
```
```bash
$ npm install
```
```bash
# development
$ npm run dev
```

```bash
# production
$ npm run build

$ npm run preview
```

___

<p align="center">
  <a href="https://www.docker.com/" target="blank"><img src="docker.png" width="200" alt="Docker Logo" /></a>
</p>

[circleci-image]: docker.png
[circleci-url]: https://www.docker.com/

## Run app with Docker

```bash
$ docker-compose up -d
```


#### Contact: [@t1ep1l0t](https://t.me/T1EP1L0T)

